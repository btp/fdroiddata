Categories:Office
License:Apache2
Web Site:http://www.feras.us/projects/markdownview
Source Code:https://github.com/falnatsheh/MarkdownView
Issue Tracker:https://github.com/falnatsheh/MarkdownView/issues

Auto Name:MarkdownView Demo
Summary:Render markdown text
Description:
Edit and render text in the markdown format.
.

Repo Type:git
Repo:https://github.com/falnatsheh/MarkdownView/

Build:1.0,1
    commit=98b0db192bfb3bf4c0fe7520c2510e569783ba58
    subdir=MarkdownViewDemo

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.0
Current Version Code:1
